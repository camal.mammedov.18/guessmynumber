const guessNum = Math.floor(Math.random()*20)

const check = document.getElementById("btn-check");
const input = document.getElementById("guess-num");
const box = document.getElementById('center')
const result = document.getElementById('result')
const firstBtn = document.getElementById('btn-again')
const attempt = document.getElementById('score')
const highScore = document.getElementById('highscore')


let count = 0;
check.addEventListener('click', function(){
    if(input.value == guessNum){
        box.innerText = guessNum
        result.innerText = 'CORRECT NUMBER'
        count ++
        result.style.color = 'seagreen'
        attempt.innerText = count
        // document.body.style.background = 'green'        
        // if(count <= 3 ){

        //     highScore.innerText = count
        // }
    }
    if(input.value < guessNum){
        result.innerText = "Too Low"
        count ++
        result.style.color = 'yellow'
        attempt.innerText = count
        
    }
    if(input.value > guessNum){
        result.innerText = "Too High"
        count ++
        result.style.color = 'yellow'
        attempt.innerText = count
    }
    if(input.value != guessNum && count == 3){
        result.innerText = "YOU Loose"
        result.style.color = 'red'
        attempt.innerText = count
        
    }
})
firstBtn.addEventListener('click', function(){
    count = 0;
    result.innerText = ""
    input.value = ""
    box.innerHTML = "?"
    attempt.innerText = '0'
})

// ? eger 3 cehdde number tapilmasa 4 cu cehd elemek olmasin